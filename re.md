## .

符合任一字元

```python
>>> re.findall("a.c", "abc")
['abc']

re.findall("a.c", "abbc")
[]
```

## *

符合0個以上的前一個字元

```python
>>> re.findall("a1*", "aaa1a1111")
['a', 'a', 'a1', 'a1111']
```

## ^

以指定字元開頭

```python
>>> re.findall("^a.*", "abc")
['abc']

>>> re.findall("^a.*", "babc")
[]
```

## $

以指定字元結尾

```python
>>> re.findall(".*a$", "bca")
['bca']

>>> re.findall(".*a$", "babc")
[]
```

## +

符合1個以上的前一個字元

```python
>>> re.findall("a1+", "aaa1a1111")
[a1', 'a1111']
```

## ?

符合0或者1個前一個字元

```python
>>> re.findall("colou?r", "color colour colouur")
['color', 'colour']

```

## *?, +?, ??

不會解釋

```python
['toys2', 'toyss2', 'toysss2']
>>> re.findall("<.*>", "<a> b <c>")
['<a> b <c>']
>>> re.findall("<.*?>", "<a> b <c>")
['<a>', '<c>']
```

## {m}

重複前一字元 m 個

```python
>>> re.findall("a{2}", "aababc")
['aa']
```

## {m,n}

重複前一字元 m~n 個

```python
>>> re.findall("a{1,3}", "aabaaaaabc")
['aa', 'aaa', 'aa']
```

## {m,n}?

看起來結果與 {m} 相同

## \

跳脫字元

## []

[] 的中任一字元

```python
>>> re.findall("[abc]d", "abcad")
['ad']
```

## (...)

只顯示條件符合 () 中的字串

```python
>>> re.findall("(re..)", "This is for python regular expression test")
['regu', 'ress']
```

## (?...)

不會用

## (?iLmsux)

不會用

## (?:...)

看起來結果與 (...) 結果相同

## (?P<name>...)

可以設定group name

```python
>>> result = re.search("(?P<gname>re..)", "This is for python regular expression test")
>>> result.group("gname")
'regu'
```
```python
import re

reg = re.compile("(?P<gname>re..)")

for result in reg.finditer("This is for python regular expression test"):
    print(result.group("gname"))

#regu
#ress
```

## (?P=name)

可以使用前一個 group name 的結果

```python
>>> re.search("(?P<gname>[\"']).*(?P=gname)", 'This is for python "regular expression" test').group()
'"regular expression"'
```

## (?#...)

```python
```

## (?=...)

如果字串後面符合指定條件時則顯示該字串

```python
>>> re.findall("Isaac (?=Asimov)", "Isaac Asimov isaac zzzz", re.I)
['Isaac ']
```

## (?!...)

如果字串後面符合指定條件時則不顯示該字串

```python
>>> re.findall("Isaac (?!Asimov)", "Isaac Asimov isaac zzzz", re.I)
['isaac ']
```

## (?<=...)

如果字串前面符合指定條件時則顯示該字串

```python
>>> re.findall("(?<=Asimov )isaac", "Asimov Isaac zzzz isaac", re.I)
['Isaac']
```

## (?<!...)

如果字串前面不符合指定條件時則顯示該字串

```python
>>> re.findall("(?<!Asimov )issac", "Asimov Issac zzzz issac", re.I)
['issac']
```

## (?(id/name)yes-pattern|no-pattern)

```python
```