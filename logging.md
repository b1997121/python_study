```python
import logging
from datetime import datetime

class MyFormatter(logging.Formatter):

    def format(self, record):
        if record.levelno == 10:
            return record.msg
        else:
            # return_msg = f"{datetime.today().strftime('%m/%d/%Y %H:%M:%S')} [{record.levelname}] [{record.module}] [{record.funcName}] {record.msg}"
            return logging.Formatter("(%(name)s) %(asctime)s [%(levelname)s] [%(module)s] [%(funcName)s] %(message)s", datefmt="%m/%d/%Y %H:%M:%S").format(record)


def log_process(name, log_name):
    root = logging.getLogger(name)
    my_format = MyFormatter()

    fhdlr = logging.FileHandler(log_name, 'w')
    fhdlr.setFormatter(my_format)

    console = logging.StreamHandler()
    console.setFormatter(my_format)

    root.setLevel(logging.DEBUG)
    root.addHandler(console)
    root.addHandler(fhdlr)

    logging.getLogger("paramiko").setLevel(logging.WARNING)
    logging.getLogger('paramiko.transport').disabled = True

    return root

def main():
    logger = log_process('test', 'run.log')
    logger.info('abx')
    logger.debug('abx')

main()
```
